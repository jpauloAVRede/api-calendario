package br.com.zup.ZupCalendAPI.services;

import br.com.zup.ZupCalendAPI.dtos.EventoDTO;
import br.com.zup.ZupCalendAPI.dtos.UsuarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventoService {

    @Autowired
    private UsuarioService usuarioService;

    private List<EventoDTO> eventos = new ArrayList<>();

    public void cadastrarEvento(EventoDTO eventoDTO, String email){

        try{
            validarDataEvento(eventoDTO.getDataInicio());

            UsuarioDTO usuarioEvento;

            usuarioEvento = this.usuarioService.pesquisaUsuario(email);

            usuarioEvento.getEventos().add(eventoDTO);
            this.eventos.add(eventoDTO);
        } catch (RuntimeException e){
            throw e;
        }

    }

    public List<EventoDTO> exibirEventos(){
        return this.eventos;
    }

    public EventoDTO pesquisaEvento(String titulo){
        //EventoDTO retornoEvento = null;

        for (EventoDTO elemLista : this.eventos) {
            if (elemLista.getTitulo().equals(titulo)){
                return elemLista;
            }
        }
        throw new RuntimeException("Não existe o evento");
    }

    public Boolean validarDataEvento(LocalDate dataCadastro){
        if (dataCadastro.isBefore(LocalDate.now())){
            throw new RuntimeException("Não é possivel criar evento com data antes de hoje");
        }
        return true;
    }


}
