package br.com.zup.ZupCalendAPI.services;

import br.com.zup.ZupCalendAPI.dtos.EventoDTO;
import br.com.zup.ZupCalendAPI.dtos.UsuarioDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService {

    private List<UsuarioDTO> usuarios = new ArrayList<>();

    public void setUsuarios(List<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public void cadastrarUsuario(UsuarioDTO usuarioDTO){
        this.usuarios.add(usuarioDTO);
    }

    public List<UsuarioDTO> exibirListaUsuario(){
        return this.usuarios;
    }

    public UsuarioDTO pesquisaUsuario(String email){
        for (UsuarioDTO elemLista : this.usuarios){
            if (elemLista.getEmail().equals(email)){
                return elemLista;
            }
        }
        throw new RuntimeException("Usuario não encontrado");
    }

    public void deletarEventoDoUsuario(UsuarioDTO usuarioAtual, String tituloEvento){
        EventoDTO eventoDeletar = null;

        for (EventoDTO elemLista : usuarioAtual.getEventos()){
            if (elemLista.getDescricao().equals(tituloEvento)) {
                eventoDeletar = elemLista;
            }
        }

        if (eventoDeletar != null){
            usuarioAtual.getEventos().remove(eventoDeletar);
        } else {
            throw new RuntimeException("Não há este evento");
        }

    }

}
