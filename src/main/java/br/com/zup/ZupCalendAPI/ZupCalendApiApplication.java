package br.com.zup.ZupCalendAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZupCalendApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupCalendApiApplication.class, args);
	}

}
