package br.com.zup.ZupCalendAPI.dtos;

import br.com.zup.ZupCalendAPI.enums.TituloEvento;

import java.time.LocalDate;

public class EventoDTO {
    private TituloEvento titulo;
    private String descricao;
    private LocalDate dataInicio;
    private LocalDate dataFim;
    private LocalDate dataCadastro = LocalDate.now();

    public EventoDTO() {
    }

    public TituloEvento getTitulo() {
        return titulo;
    }

    public void setTitulo(TituloEvento titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public LocalDate getDataFim() {
        return dataFim;
    }

    public void setDataFim(LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

}
