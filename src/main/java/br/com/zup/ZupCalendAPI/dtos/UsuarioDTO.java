package br.com.zup.ZupCalendAPI.dtos;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDTO {
    private String nome;
    private String email;
    private List<EventoDTO> eventos = new ArrayList<>();

    public UsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<EventoDTO> getEventos() {
        return eventos;
    }

    public void setEventos(List<EventoDTO> eventos) {
        this.eventos = eventos;
    }

}
