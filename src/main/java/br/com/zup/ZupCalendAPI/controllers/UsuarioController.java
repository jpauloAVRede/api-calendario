package br.com.zup.ZupCalendAPI.controllers;

import br.com.zup.ZupCalendAPI.dtos.EventoDTO;
import br.com.zup.ZupCalendAPI.dtos.UsuarioDTO;
import br.com.zup.ZupCalendAPI.services.EventoService;
import br.com.zup.ZupCalendAPI.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private EventoService eventoService;

    @PostMapping
    public void acionaCadastroUsuario(@RequestBody UsuarioDTO usuarioDTO) {
        this.usuarioService.cadastrarUsuario(usuarioDTO);
    }

    @GetMapping
    public List<UsuarioDTO> acionaExibirListaUsuario() {
        return this.usuarioService.exibirListaUsuario();
    }

    @GetMapping("/usuarios/{email}")
    public void acionaAdicionarEventoUsuario(@PathVariable String email, @PathVariable String titulo){
        EventoDTO eventoCad = null;
        UsuarioDTO usuaCad = null;

        eventoCad = this.eventoService.pesquisaEvento(titulo);
        usuaCad = this.usuarioService.pesquisaUsuario(email);

        usuaCad.getEventos().add(eventoCad);
    }

    @DeleteMapping("/usuario/{email}/{tituloEvento}")
    public void chamarDeletarEventoUsuario(@PathVariable String email, @PathVariable String tituloEvento){
        try {
            UsuarioDTO usuarioAtual;
            usuarioAtual = usuarioService.pesquisaUsuario(email);
            usuarioService.deletarEventoDoUsuario(usuarioAtual, tituloEvento);
        } catch (RuntimeException e){
            throw new RuntimeException("Não foi possível deletar o evento");
        }
    }

}
