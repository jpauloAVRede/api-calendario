package br.com.zup.ZupCalendAPI.controllers;

import br.com.zup.ZupCalendAPI.dtos.EventoDTO;
import br.com.zup.ZupCalendAPI.services.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/agenda")
public class EventoController {

    @Autowired
    private EventoService eventoService;

    @PostMapping("/evento/{email}")
    public void chamarCadastrarEvento(@RequestBody EventoDTO eventoDTO, @PathVariable String email){
        this.eventoService.cadastrarEvento(eventoDTO, email);
    }

    @GetMapping
    public List<EventoDTO> chamaExibirListaEventos(){
        return this.eventoService.exibirEventos();
    }


}
